*** Settings ***
Library  Selenium2Library

*** Variables ***
${BROWSER}          Chrome
${HOST}             https://ricamo.info

*** Keywords ***
Prepare Test Environment
    Open Browser  ${HOST}  ${BROWSER}
    Maximize Browser Window

Enter link  [Arguments]  ${linkname}  ${expectedpage}
    Click link  ${linkname}
    Page Should Contain Element  ${expectedpage}

Enter link expect Element Text  [Arguments]  ${linkname}  ${expectedelem}  ${expectedtext}
    Click link  ${linkname}
    Element Should Contain   ${expectedelem}  ${expectedtext}

Enter link by xPath  [Arguments]  ${elxpath}  ${expectedpage}
    Click Element  ${elxpath}
    Page Should Contain Element  ${expectedpage}

Enter Partial link  [Arguments]  ${linkname}  ${expectedpage}
    Click Partial link  ${linkname}
    Page Should Contain Element  ${expectedpage}

Enter link expect Page Text  [Arguments]  ${linkname}  ${expectedtext}
    Click link  ${linkname}
    Page Should Contain  ${expectedtext}