*** Settings ***
Documentation  Test for Signin with login and password to ricamo.info
Resource  ../Resourses/ricamo_resource.robot
Test Setup  Prepare Test Environment
Test Teardown  Close All Browsers

*** Variables ***


*** Test Cases ***
User must sign in to check out
    [Documentation]  Test for Signin with login and password to ricamo.info
    [Tags]  Smoke
    Wait Until Page Contains Element  css=.common-home
    click Element  css=.btn-login
    Wait Until Page Contains  Авторизація
    Input Text    input-email  nrg.xiii@gmail.com
    Input password  input-password    testrpwd
    Click Element    css=input.btn-primary
    Page Should Contain  Мій обліковий запис
    Click link  Вийти
    Page Should Contain  Ви вийшли з вашого Особистого Кабінету.