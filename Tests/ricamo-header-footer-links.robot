*** Settings ***
Documentation  This is some basic info about the whole suite
Resource  ../Resourses/ricamo_resource.robot
Test Setup  Prepare Test Environment
Test Teardown  Close All Browsers

*** Variables ***


*** Test Cases ***
Header and footer links Test
    [Documentation]  Ricamo.info link test
    [Tags]  Smoke

    Enter link by xPath  xpath=(//*[@id="menu"]/div/ul/li)[2]  css=.product-allproduct
    Enter link  Акції  css=.product-special
    Enter link  Майстер-клас  css=.news-ncategory
    Enter link  Новини  css=.news-ncategory
    Enter link  Контакти  css=.information-contact
    Enter link  Головна  css=.common-home

    Enter link  увійти  css=.account-login
    Enter link  зареєструватися  css=.account-simpleregister
    Enter link expect Element Text  Про нас  css=div#content  Про нас
    Enter link expect Element Text  Для інтернет магазинів  css=div#content  Для інтернет магазинів
    Enter link expect Element Text  Доставка і оплата  css=div#content  Доставка і оплата
    Enter link expect Element Text  Контакти  css=div#content  Зв’язатися з нами

Footer sociad links test
    [Documentation]  ricamo.info outer links(Target="_blank") test
    [Tags]  Smoke

    Enter link expect Page Text  Facebook  Ricamo
    Enter link expect Page Text  вконтакті  Ricamo
    Enter link expect Page Text  Одноклассники  Ricamo